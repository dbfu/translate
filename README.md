## 1.选中要翻译的文字
![avatar](https://gitlab.com/dbfu/translate/raw/master/images/01.png)

## 2.右键选择快速翻译功能
![avatar](https://gitlab.com/dbfu/translate/raw/master/images/02.png)

## 3.编辑器下面会出现翻译结果，以及一些操作选项
1. 选择是会进入下一步
2. 选择否会取消
3. 选择编辑结果可以对翻译结果进行修改
![avatar](https://gitlab.com/dbfu/translate/raw/master/images/03.png)

## 4.选择是后，会弹出一个输入框，这里输入的是多语言的key，可以更改
![avatar](https://gitlab.com/dbfu/translate/raw/master/images/04.png)

## 5.输入完多语言的key，然后回车就会出现一个选择模块的输入框
![avatar](https://gitlab.com/dbfu/translate/raw/master/images/05.png)

## 6.选择一个模块，然后回车，就完成了，翻译后的结果也会自动存入库中。
![avatar](https://gitlab.com/dbfu/translate/raw/master/images/06.png)

# 提示：
  ## 选择要翻译的文字，然后用快捷键也可以
  * mac快捷键是cmmand+option+t
  * windows快捷键是control+alt+t
