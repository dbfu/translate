/* eslint-disable */

const vscode = require('vscode');

// this method is called when your extension is activated
// your extension is activated the very first time the command is executed
const modules = ["accounting", "auth", "asset", "base", "budget", "contract", "expense", "fund", "haep", "job", "mdata", "payment", "prepayment", "peripheral", "tax", "workbench", "workflow"];
let url = '';

/**
 * @param {vscode.ExtensionContext} context
 */
function activate(context) {

  const disposable = vscode.commands.registerCommand('extension.quick_translate', function () {
    // 从配置里获取后台的url
    url = vscode.workspace.getConfiguration().get('Hcf Translate Url');
    main();
  });

  const disposable1 = vscode.commands.registerCommand('extension.quick_translate1', function () {
    // 从配置里获取后台的url
    url = vscode.workspace.getConfiguration().get('Hcf Translate Url');
    main(true);
  });

  context.subscriptions.push(disposable);
  context.subscriptions.push(disposable1);
}
exports.activate = activate;

// this method is called when your extension is deactivated
function deactivate() { }

function main(flag) {
  // 获取当前编辑器
  const activeTextEditor = vscode.window.activeTextEditor;
  // 获取当前编辑器的内容
  const activeDocument = activeTextEditor.document;
  // 获取选中的范围
  const selection = activeTextEditor.selection;
  // 获取选中的文本
  let text = activeDocument.getText(new vscode.Range(selection.start, selection.end));
  // 去除开始的冒号
  if (text[0] === "'" || text[0] === '"') {
    text = text.substring(1);
  }
  // 去除结束的冒号
  if (text[text.length - 1] === "'" || text[text.length - 1] === '"') {
    text = text.substring(0, text.length - 1);
  }

  // 到后台查看公共模块有没有重复的数据
  require('axios')({
    url: `${url}/base/util/query/front/locale`,
    method: "GET",
    params: {
      description: text
    }
  }).then(({ data }) => {
    // 如果有重复的数据，就不在翻译，用后台查出来的数据。
    if (data) {
      // 提示消息
      vscode.window.showInformationMessage('检测到公共模块有此文本的多语言，将直接采用公共模块的多语言。');
      // 替换选中的内容
      vscode.window.activeTextEditor.edit(editBuilder => {
        let txt = `this.$t('${data.keyCode}')`;
        if (flag) {
          txt = `{this.$t('${data.keyCode}')}`;
        }
        editBuilder.replace(selection, txt);
      });
      return;
    }
    // 调用百度翻译接口翻译
    translate(text).then(({ data }) => {
      const { trans_result } = data;
      let dst = trans_result[0].dst;

      // 把单词首字母变成大写 比如翻译返回的结果是 hello world 这个方法转换成 Hello World 
      if (dst.indexOf(" ") >= 0) {
        dst = dst.split(" ").map(item => {
          const first = item[0].toUpperCase();
          return first + item.substring(1);
        }).join(" ");
      };

      vscode.window.showInformationMessage(`是否采用翻译结果？${dst}`, '是', '否', '编辑结果').then(result => {
        if (result === '是') {
          // 这里是对翻译的结果格式化
          format(dst, selection, text, flag);
        } else if (result === "编辑结果") {
          // 弹出一个编辑框，对翻译的结果进行编辑
          vscode.window.showInputBox({
            value: dst,
          }).then(value => {
            if (!value) return;
            format(value, selection, text, flag);
          })
        } else { }
      });
    }).catch(error => {
      vscode.window.showErrorMessage('翻译失败');
      console.log(error.response.data);
    });
  })

  // activeTextEditor.selection = new vscode.Selection(new vscode.Position(0, 0), new vscode.Position(0, 2))
}

// 把翻译后的数据保存到数据库
function save(key, value1, value2, module) {
  return require('axios')({
    url: `${url}/base/api/implement/front/locale/init`,
    method: "POST",
    data: [{
      applicationCode: module,
      language: "zh_cn",
      keyDescription: value1,
      keyCode: key,
    },
    {
      applicationCode: module,
      language: "en_us",
      keyDescription: value2,
      keyCode: key,
    }],
  });
}

// 格式化翻译结果
function format(val, selection, source, flag) {
  let dst = val;
  // 把翻译结果单词首字母变成小写 然后中间用.隔开
  if (dst.indexOf(" ") >= 0) {
    dst = dst.split(" ").map(item => item.toLowerCase()).join(".");
  }
  // 弹出一个输入框，输入多语言的key
  vscode.window.showInputBox({
    placeHolder: "输入多语言的key",
    value: dst.toLowerCase(),
  }).then(value => {
    if (!value) return;
    // 显示一个选择框
    vscode.window.showQuickPick(modules, { placeHolder: "选择模块" }).then(res => {
      if (!res) return;
      // 把翻译结果保存到数据库
      save(`${res}.${value}`, source, val, res).then(() => {
        vscode.window.showInformationMessage("多语言保存成功!");
        // 用多语言的key替换选中的内容
        vscode.window.activeTextEditor.edit(editBuilder => {
          let txt = `this.$t("${res}.${value}")`;
          if (flag) {
            txt = `{this.$t("${res}.${value}")}`;
          }
          editBuilder.replace(selection, txt);
        });
      }).catch(() => {
        vscode.window.showErrorMessage("多语言保存失败,请重试！");
      })
    });
  });
}

/**
 * 翻译
 * @param {搜索关键字} keyword 
 */
function translate(keyword) {
  var appid = '20180824000198430';
  var key = 'QQhohHQNBDcKFVmkIK_r';
  var salt = (new Date).getTime();
  var from = 'zh';
  var to = 'en';
  var str1 = appid + keyword + salt + key;
  var sign = require('./md5')(str1);

  return require("axios")({
    url: 'http://api.fanyi.baidu.com/api/trans/vip/translate',
    params: {
      q: keyword,
      appid,
      salt,
      from,
      to,
      sign,
    },
    method: "GET",
  });
}

module.exports = {
  activate,
  deactivate
}
